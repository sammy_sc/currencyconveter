import Foundation
import Utilities

public struct ExchangeRatesResource: Resource {

  // MARK: -

  public var parameters: [String: String]?
  public var url: URL = URL(safeString: "https://revolut.duckdns.org/latest")

  // MARK: -

  public init(base: String) {
    parameters = [
      "base": base
    ]
  }
}

extension ExchangeRatesResource {
  public struct Model: Decodable {
    public let base: String
    public let rates: [String: Double]
  }
}
