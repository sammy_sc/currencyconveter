import Foundation

public protocol Resource {
  var url: URL { get }
  var parameters: [String: String]? { get }
  associatedtype Model: Decodable
}
