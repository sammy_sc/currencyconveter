import Foundation
import RxSwift

public final class WebService: WebServiceType {
  private let session: URLSession = URLSession(configuration: .ephemeral)

  public func get<T: Resource>(_ resource: T) -> Single<T.Model> {
    return Single.create { single in
      var component = URLComponents(url: resource.url, resolvingAgainstBaseURL: false)

      component?.queryItems = resource.parameters?.map {
        URLQueryItem(name: $0.key, value: $0.value)
      }

      let request = URLRequest(url: component?.url ?? resource.url)

      print("making network call to \(resource.url)")
      let task = self.session.dataTask(with: request) { data, _, e in
        print("network call to \(resource.url) finished")
        if let error = e {
          return DispatchQueue.main.async {
            single(.error(error))
          }
        }
        if let data = data {
          do {
            let decoder = JSONDecoder()
            let decoded = try decoder.decode(T.Model.self, from: data)
            DispatchQueue.main.async {
              single(.success(decoded))
            }
          } catch {
            DispatchQueue.main.async {
              single(.error(error))
            }
          }
        }
      }

      task.resume()

      return Disposables.create {
        task.cancel()
      }
    }
  }

  public init() { }
}
