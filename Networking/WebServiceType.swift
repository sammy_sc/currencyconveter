import RxSwift

public protocol WebServiceType {
  func get<T: Resource>(_ resource: T) -> Single<T.Model>
}
