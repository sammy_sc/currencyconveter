# iOS Sample App

![Platforms](https://img.shields.io/badge/platform-iOS-lightgrey.svg)
[![Swift Version](https://img.shields.io/badge/Swift-4.2-F16D39.svg?style=flat)](https://developer.apple.com/swift)
[![Twitter](https://img.shields.io/badge/twitter-@SamuelSusla-blue.svg)](http://twitter.com/SamuelSusla)

## Getting started

To get started with the project run `./bootstrap.sh` to install Carthage, SwifLint and build all the Carthage dependencies. 

## Architecture concepts
- MVVM
- Dependency Injection
- Data Binding (using [RxSwift](https://github.com/ReactiveX/RxSwift))
- Dependencies management (using [Carthage](https://github.com/Carthage/Carthage))

## Requirements 
- [Carthage](https://github.com/Carthage/Carthage)
- XCode 10

#### Optional

* [SwifLint](https://github.com/realm/SwiftLint)

## Tests

- Snapshot tests will only work on iPhone X

## Further improvements

- If the app had more screens, use Coordinators to handle transitions
- State restoration
- Make snapshot tests for more screen sizes

Any suggestions for improvements are welcome. If you have any problems with the project, don't hesitate to contact me.

- [Twitter](http://twitter.com/SamuelSusla)
- samuel.susla@gmail.com