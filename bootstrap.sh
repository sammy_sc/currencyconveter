#!/bin/bash

brew update
brew outdated carthage || brew upgrade carthage
brew outdated swiftlint || brew upgrade swiftlint

carthage bootstrap --platform iOS