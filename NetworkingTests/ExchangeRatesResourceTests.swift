import XCTest
@testable import Networking

final class ExchangeRatesResourceTests: XCTestCase {
    func testDecoding() {
      let json: ExchangeRatesResource.Model? = decodeJSON("rates")

      XCTAssertEqual(json?.base, "EUR")
      XCTAssertEqual(json?.rates, [
        "AUD": 1.6214,
        "BGN": 1.9619,
        "BRL": 4.8068
      ])
    }
}
