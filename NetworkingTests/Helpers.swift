import XCTest
import Foundation

private final class ClassInThisBundle { }

/// Tries to decode file with provided name using decodable protocol on `T`.
///
/// - Parameters:
///   - fileName: to be decoded.
///   - file: not to be changed as this will provide better error message.
///   - line: not to be changed as this will provide better error message.
/// - Returns: json file decoded into `T`.
func decodeJSON<T: Decodable>(_ fileName: String, file: StaticString = #file, line: UInt = #line) -> T? {
  let bundle = Bundle.init(for: ClassInThisBundle.self)

  guard let fileURL = bundle.url(forResource: fileName, withExtension: "json")
    else {
      XCTFail("Failed to construct URL", file: file, line: line)
      return nil
  }

  guard let data = try? Data(contentsOf: fileURL) else {
    XCTFail("Failed to open the file", file: file, line: line)
    return nil
  }

  guard let decoded = try? JSONDecoder().decode(T.self, from: data)
    else {
      XCTFail("Failed to decode the json", file: file, line: line)
      return nil
  }

  return decoded
}
