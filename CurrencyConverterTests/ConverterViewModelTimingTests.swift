import XCTest
import RxSwift
import RxBlocking
import RxCocoa
import RxTest
@testable import CurrencyConverter

private let rates = ExchangeRates(
  base: "USD",
  rates: ["SGD": 1.38]
)

private let rates2 = ExchangeRates(
  base: "USD",
  rates: ["SGD": 1.39, "CZK": 22.137]
)

let error = NSError(domain: "TestError", code: 0)

final class ConverterViewModelTimingTests: XCTestCase {
  private var dataService: DataServiceMock!
  private var vm: ConverterViewModel!
  private var testScheduler: TestScheduler!
  override func setUp() {
    super.setUp()
    testScheduler = TestScheduler(initialClock: 0)
    dataService = DataServiceMock()
    vm = ConverterViewModel(
      dataService: dataService,
      scheduler: testScheduler
    )
    vm.start()
  }

  func testReoccuringDataServiceRequest() {
    let firstObserver = testScheduler.createObserver([ConverterRowModel].self)
    var disposable1: Disposable!

    SharingScheduler.mock(scheduler: testScheduler) {
      disposable1 = vm.sections.map { $0.first!.items }.asObservable().subscribe(firstObserver)

      testScheduler.scheduleAt(0) {
        XCTAssertEqual(self.dataService._getCallCount, 1)
      }

      testScheduler.scheduleAt(1) {
        self.dataService._singleEvent?(.success(rates))
        XCTAssertEqual(firstObserver.events.count, 2)
      }

      testScheduler.scheduleAt(4) {
        XCTAssertEqual(self.dataService._getCallCount, 2)
      }

      testScheduler.scheduleAt(5) {
        XCTAssertEqual(firstObserver.events.count, 2)
        self.dataService._singleEvent?(.success(rates2))
      }

      testScheduler.scheduleAt(6) {
        XCTAssertEqual(firstObserver.events.count, 3)
      }

      testScheduler.scheduleAt(8) {
        XCTAssertEqual(self.dataService._getCallCount, 3)
      }

      testScheduler.scheduleAt(9) {
        // Error is produced, we expect the view model to retry.
        self.dataService._singleEvent?(.error(error))
      }

      testScheduler.scheduleAt(12) {
        XCTAssertEqual(self.dataService._getCallCount, 4)
      }

      testScheduler.scheduleAt(13) {
        disposable1.dispose()
      }

      testScheduler.start()
    }

    XCTAssertEqual(firstObserver.events.count, 3)
  }

  func testCallingStopStart() {
    let firstObserver = testScheduler.createObserver([ConverterRowModel].self)
    var disposable1: Disposable!

    SharingScheduler.mock(scheduler: testScheduler) {
      disposable1 = vm.sections.map { $0.first!.items }.asObservable().subscribe(firstObserver)

      testScheduler.scheduleAt(1) {
        self.dataService._singleEvent?(.success(rates))
        XCTAssertEqual(firstObserver.events.count, 2)
      }

      testScheduler.scheduleAt(1) {
        self.vm.stop()
      }

      testScheduler.scheduleAt(250) {
        XCTAssertEqual(self.dataService._getCallCount, 1)
      }

      testScheduler.scheduleAt(251) {
        self.vm.start()
        XCTAssertEqual(self.dataService._getCallCount, 2)
      }

      testScheduler.scheduleAt(252) {
        disposable1.dispose()
      }

      testScheduler.start()
    }
  }
}

private extension ConverterViewModelTimingTests {

  private func getRows() -> [ConverterRowModel]? {
    let sections = try! vm.sections.toBlocking().first()!

    XCTAssertEqual(sections.count, 1)

    guard let items = sections.first?.items else {
      XCTFail("items must not be nil")
      return nil
    }

    return items
  }
}
