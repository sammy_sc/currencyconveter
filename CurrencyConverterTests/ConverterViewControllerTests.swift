import XCTest
import RxSwift
import RxBlocking
import Snap
@testable import CurrencyConverter

final class ConverterViewControllerTests: XCTestCase {
  var vm: ConverterViewModelMock!
  var vc: ConverterViewController!
  var bag: DisposeBag!
  override func setUp() {
    super.setUp()
    bag = DisposeBag()
    vm = ConverterViewModelMock()
    vc = ConverterViewController(vm)
  }

  func testInitialLoad() {
    expect(vc).toMatchSnapshot()
  }

  func testViewModelStart() {
    vc.loadProgrammatically()
    XCTAssertEqual(vm._startCallCount, 1)
  }

  func testTitle() {
    vc.loadProgrammatically()
    vm.title.accept("Loading")
    XCTAssertEqual(vc.title, "Loading")
    vm.title.accept("Random Value")
    XCTAssertEqual(vc.title, "Random Value")
  }

  func testLoadWithThreeRows() {
    let usd = ConverterRowModel(iso: "USD", value: 1.0)
    let czk = ConverterRowModel(iso: "CZK", value: 22.137)
    let sgd = ConverterRowModel(iso: "SGD", value: 1.38)

    let section = CurrencySection(
      items: [usd, czk, sgd]
    )

    vm.sections.accept([section])

    expect(vc).toMatchSnapshot(named: "before update")

    /// Update one of the values to see that bindings work.
    sgd.exchangeRate.value = 100

    expect(vc).toMatchSnapshot(named: "after update")
  }
}
