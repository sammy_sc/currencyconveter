import XCTest
@testable import CurrencyConverter

final class ExchangeRatesTests: XCTestCase {

    func testChangingBase() {
      var rates = ExchangeRates(
          base: "USD",
          rates: ["SGD": 1.38, "CZK": 22.137]
      )

      XCTAssertEqual(rates.base, "USD")
      XCTAssertEqual(rates.rates["SGD"], 1.38)
      XCTAssertEqual(rates.rates["USD"], nil)

      rates.base = "SGD"

      XCTAssertEqual(rates.rates["SGD"], nil)

      beClose(rates.rates["USD"]!, 0.7246)
      beClose(rates.rates["CZK"]!, 16.0413)
    }
}
