import Foundation

extension RunLoop {
  /// Blocks the current queue for specified time.
  static func wait(seconds: Double = 1.0) {
    let date = Date(timeIntervalSinceNow: seconds)
    RunLoop.current.run(until: date)
  }
}
