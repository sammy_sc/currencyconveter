import Foundation
import XCTest

/// Tests two doubles whether they are close
/// to each other with cetrain precision.
func beClose(_ lhs: Double,
             _ rhs: Double,
             precision: Double = 0.0001,
             file: StaticString = #file,
             line: UInt = #line) {
  if abs(lhs - rhs) < precision {
    return
  } else {
    XCTFail(
      "\(lhs) is not close enough to \(rhs) with precision: \(precision)",
      file: file,
      line: line
    )
  }
}
