import UIKit

extension UIViewController {
  /// Calls view controller life cycle methods.
  func loadProgrammatically() {
    beginAppearanceTransition(true, animated: false)
    endAppearanceTransition()
  }
}
