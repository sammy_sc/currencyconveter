@testable import CurrencyConverter
import RxSwift
import RxCocoa

final class ConverterViewModelMock: ConverterViewModelType {
  var title = BehaviorRelay<String>(value: "")
  var sections = BehaviorRelay<[CurrencySection]>(value: [])
  var didSelect = PublishSubject<ConverterRowModel>()
  var value = BehaviorRelay<String?>(value: "")

  var _startCallCount = 0
  func start() {
    _startCallCount += 1
  }

  var _stopCallCount = 0
  func stop() {
    _stopCallCount += 1
  }
}
