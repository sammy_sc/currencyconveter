import Foundation
import RxSwift
@testable import CurrencyConverter

final class DataServiceMock: ConverterDataServiceType {
  var _getCallCount = 0
  var _arguments = [CurrencyISO]()
  var _singleEvent: ((SingleEvent<ExchangeRates>) -> Void)?

  func getExchangeRates(for base: CurrencyISO) -> Single<ExchangeRates> {
    _arguments.append(base)
    _getCallCount += 1
    return Single.create { single in
      self._singleEvent = single
      return Disposables.create()
    }
  }

  init() { }
}
