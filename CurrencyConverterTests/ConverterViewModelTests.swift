import XCTest
import RxSwift
import RxBlocking
@testable import CurrencyConverter

private let rates = ExchangeRates(
  base: "USD",
  rates: ["SGD": 1.38, "CZK": 22.137]
)

final class ConverterViewModelTests: XCTestCase {
  private var dataService: DataServiceMock!
  private var vm: ConverterViewModel!
  override func setUp() {
    super.setUp()
    dataService = DataServiceMock()
    vm = ConverterViewModel(dataService: dataService)
    vm.start()
  }

  override func tearDown() {
    super.tearDown()
    vm.stop()
  }

  func testInitialValue() {
    let value = try! vm.value.toBlocking().first()!
    XCTAssertEqual(value, "1.0")
  }

  func testInitialNetworkCall() {
    XCTAssertEqual(dataService._getCallCount, 1)
  }

  func testTitle() {
    let title = try! vm.title.toBlocking().first()!
    XCTAssertEqual(title, "Loading")

    dataService._singleEvent?(.success(rates))

    let titleAfterLoading = try! vm.title.toBlocking().first()!

    XCTAssertEqual(titleAfterLoading, "")
  }

  func testInitialSections() {
    let sections = try! vm.sections.toBlocking().first()!

    XCTAssertEqual(sections.count, 1)

    let items = sections.first?.items

    XCTAssertEqual(items?.count, 0)
  }

  func testChangeOfBase() {
    dataService._singleEvent?(.success(rates))

    assertInitialSections()

    let sections = try! vm.sections.toBlocking().first()!

    guard let items = sections.first?.items else {
      return XCTFail("items must not be nil")
    }

    vm.didSelect.onNext(items[2])

    /// One second has not passed, therefore network call has not been made.
    XCTAssertEqual(dataService._getCallCount, 1)

    let changedItems = getRows()!

    /// Order changes and exchanges rates are recalculated.
    XCTAssertEqual(changedItems[0].iso, "SGD")
    beClose(changedItems[0].exchangeRate.value, 1.38)
    XCTAssertEqual(changedItems[1].iso, "USD")
    beClose(changedItems[1].exchangeRate.value, 1)
    XCTAssertEqual(changedItems[2].iso, "CZK")
    beClose(changedItems[2].exchangeRate.value, 22.137)
  }

  func testChangeValue() {
    dataService._singleEvent?(.success(rates))

    assertInitialSections()

    vm.value.accept("2.0")

    let changedSections = try! vm.sections.toBlocking().first()!

    guard let changedItems = changedSections.first?.items else {
      return XCTFail("items must not be nil")
    }

    // Orders of rows has not changed but values have been recalculated.
    XCTAssertEqual(changedItems[1].iso, "CZK")
    beClose(changedItems[1].exchangeRate.value, 22.137 * 2)
    XCTAssertEqual(changedItems[2].iso, "SGD")
    beClose(changedItems[2].exchangeRate.value, 1.38 * 2)

    /// One second has not passed, therefore network call has not been made.
    XCTAssertEqual(dataService._getCallCount, 1)
  }

  /// These tests are block the main queue for specific time and check the state of dataService.
  /// There are more extensive tests for this logic in `ConverterViewModelTimingTests.swift`
  func testReoccuringDataServiceRequest() {
    let waitTime: TimeInterval = 1.1
    XCTAssertEqual(dataService._getCallCount, 1)
    XCTAssertEqual(dataService._arguments, ["USD"])
    dataService._singleEvent?(.success(rates))
    RunLoop.wait(seconds: waitTime)
    XCTAssertEqual(dataService._getCallCount, 2)
    XCTAssertEqual(dataService._arguments, ["USD", "USD"])
    RunLoop.wait(seconds: waitTime)
    // Previous request has not finished, therefore new request
    // is not triggered.
    XCTAssertEqual(dataService._getCallCount, 2)

    /// We are changing base.
    vm.didSelect.onNext(getRows()![1])
    dataService._singleEvent?(.success(rates))

    RunLoop.wait(seconds: waitTime)
    XCTAssertEqual(dataService._getCallCount, 3)
    XCTAssertEqual(dataService._arguments, ["USD", "USD", "CZK"])
  }

  /// These tests are block the main queue for specific time and check the state of dataService.
  /// There are more extensive tests for this logic in `ConverterViewModelTimingTests.swift`
  func testStopStart() {
    vm.stop()
    let waitTime: TimeInterval = 1.1
    XCTAssertEqual(dataService._getCallCount, 1)
    dataService._singleEvent?(.success(rates))
    RunLoop.wait(seconds: waitTime)
    XCTAssertEqual(dataService._getCallCount, 1)
    vm.start()
    XCTAssertEqual(dataService._getCallCount, 2)
  }
}

private extension ConverterViewModelTests {

  private func getRows() -> [ConverterRowModel]? {
    let sections = try! vm.sections.toBlocking().first()!

    XCTAssertEqual(sections.count, 1)

    guard let items = sections.first?.items else {
      XCTFail("items must not be nil")
      return nil
    }

    return items
  }
  private func assertInitialSections() {
    let sections = try! vm.sections.toBlocking().first()!

    XCTAssertEqual(sections.count, 1)

    guard let items = sections.first?.items else {
      return XCTFail("items must not be nil")
    }

    XCTAssertEqual(items.count, 3)

    /// Except first row, rows are alphabetically ordered..
    XCTAssertEqual(items[0].iso, "USD")
    beClose(items[0].exchangeRate.value, 1)
    XCTAssertEqual(items[1].iso, "CZK")
    beClose(items[1].exchangeRate.value, 22.137)
    XCTAssertEqual(items[2].iso, "SGD")
    beClose(items[2].exchangeRate.value, 1.38)
  }
}
