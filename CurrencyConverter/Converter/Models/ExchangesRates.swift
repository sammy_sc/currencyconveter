import Foundation

typealias CurrencyISO = String
typealias ExchangeRate = Double

struct ExchangeRates {
  /// Base for the exchange rates, if changed rates
  /// will be recalculated on best effort basis.
  public var base: CurrencyISO {
    didSet {
      if oldValue != base {
        recalculate(oldBase: oldValue)
      }
    }
  }

  private(set) var rates: [CurrencyISO: ExchangeRate]

  init(base: CurrencyISO, rates: [CurrencyISO: ExchangeRate]) {
    self.base = base
    self.rates = rates
  }

  /// Recalculates rates for new base.
  private mutating func recalculate(oldBase: CurrencyISO) {
    let newBaseExchange = rates[base] ?? 1.0
    rates.removeValue(forKey: base)

    rates = rates.reduce(into: [:]) { (result, element: (key: CurrencyISO, value: ExchangeRate)) in
      result[element.key] = element.value / newBaseExchange
    }

    rates[oldBase] = 1 / newBaseExchange
  }
}
