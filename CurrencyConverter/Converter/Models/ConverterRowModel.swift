import RxSwift
import RxDataSources

struct ConverterRowModel {
  let iso: CurrencyISO
  var exchangeRate: Variable<ExchangeRate>

  init(iso: CurrencyISO, value: ExchangeRate) {
    self.iso = iso
    exchangeRate = Variable(value)
  }
}

extension ConverterRowModel: Equatable {
  static func == (lhs: ConverterRowModel, rhs: ConverterRowModel) -> Bool {
    return lhs.iso == rhs.iso
  }
}

extension ConverterRowModel: IdentifiableType {
  public var identity: CurrencyISO {
    return iso
  }
}
