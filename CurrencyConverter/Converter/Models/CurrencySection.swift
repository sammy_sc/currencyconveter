import RxDataSources

struct CurrencySection {
  var header: String
  var items: [ConverterRowModel]
}

extension CurrencySection: AnimatableSectionModelType {
  var identity: String {
    return header
  }

  init(original: CurrencySection, items: [ConverterRowModel]) {
    self = original
    self.items = items
  }

  init(items: [ConverterRowModel]) {
    self.items = items
    self.header = "Currencies"
  }
}
