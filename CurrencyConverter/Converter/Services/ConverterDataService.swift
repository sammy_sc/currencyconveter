import RxSwift
import Networking

protocol ConverterDataServiceType {
  func getExchangeRates(for base: CurrencyISO) -> Single<ExchangeRates>
}

final class ConverterDataService: ConverterDataServiceType {
  private let webService: WebServiceType

  init(webService: WebServiceType) {
    self.webService = webService
  }

  func getExchangeRates(for base: CurrencyISO) -> Single<ExchangeRates> {
    let resource = ExchangeRatesResource(base: base)

    return webService.get(resource)
      .map {
      ExchangeRates(base: $0.base, rates: $0.rates)
    }
  }
}
