import UIKit
import SnapKit
import RxCocoa
import RxSwift

final class ConverterCell: UITableViewCell {

  var icon: UIImage? {
    get {
      return iconView.image
    } set {
      iconView.image = newValue
    }
  }

  var title: String? {
    get {
      return titleLabel.text
    } set {
      titleLabel.text = newValue
    }
  }

  var subtitle: String? {
    get {
      return subtitleLabel.text
    } set {
      subtitleLabel.text = newValue
    }
  }

  private var bag = DisposeBag()

  func bind(to propery: Observable<String?>) {
    propery
      .bind(to: textField.rx.text)
      .disposed(by: bag)

    propery
      .map { $0 == nil || $0 == "0" }
      .map { t -> UIColor in
        if t {
          return .gray
        } else {
          return .black
        }
    }.subscribe(onNext: { color in
      self.textField.textColor = color
    }).disposed(by: bag)
  }

  private var inputDisposable: Disposable?

  func bindInput(to property: BehaviorRelay<String?>) {
    inputDisposable = textField.rx.text
      .bind(to: property)
  }

  // MARK: -

  private let iconView: UIImageView = {
    let v = UIImageView()
    v.clipsToBounds = true
    v.contentMode = .scaleAspectFill
    v.layer.cornerRadius = 12.5
    v.backgroundColor = .gray
    return v
  }()

  private let stack: UIStackView = {
    let s = UIStackView()
    s.axis = .vertical
    s.alignment = .leading
    return s
  }()

  private let titleLabel: UILabel = {
    let l = UILabel()
    l.textColor = .black
    l.font = .preferredFont(forTextStyle: .caption1)
    return l
  }()

  private let subtitleLabel: UILabel = {
    let l = UILabel()
    l.textColor = .gray
    l.font = .preferredFont(forTextStyle: .caption2)
    return l
  }()

  private let textField: UITextField = {
    let v = UITextField()
    v.keyboardType = .decimalPad
    v.isUserInteractionEnabled = false
    return v
  }()

  // MARK: -

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    selectionStyle = .none

    stack.addArrangedSubview(titleLabel)
    stack.addArrangedSubview(subtitleLabel)

    contentView.addSubview(iconView)
    contentView.addSubview(stack)
    contentView.addSubview(textField)

    iconView.snp.makeConstraints { m in
      m.size.equalTo(CGSize(width: 25, height: 25))
      m.leading.equalTo(contentView).offset(16)
      m.centerY.equalTo(contentView)
    }

    stack.snp.makeConstraints { m in
      m.leading.equalTo(iconView.snp.trailing).offset(16)
      m.centerY.equalTo(contentView)
      m.trailing.lessThanOrEqualTo(textField.snp.leading).offset(16)
    }

    textField.snp.makeConstraints { m in
      m.trailing.equalTo(contentView).offset(-16)
      m.centerY.equalTo(contentView)
      m.leading.greaterThanOrEqualTo(contentView.snp.centerX)
    }
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  override func prepareForReuse() {
    super.prepareForReuse()
    bag = DisposeBag()
    textField.textColor = .black
    inputDisposable?.dispose()
  }

  override func becomeFirstResponder() -> Bool {
    print(#function)
    textField.isUserInteractionEnabled = true
    return textField.becomeFirstResponder()
  }

  override func resignFirstResponder() -> Bool {
    inputDisposable?.dispose()
    textField.isUserInteractionEnabled = false
    return textField.resignFirstResponder()
  }
}
