import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import FlagKit
import Utilities

protocol ConverterViewModelType {
  var title: BehaviorRelay<String> { get }
  var sections: BehaviorRelay<[CurrencySection]> { get }
  var didSelect: PublishSubject<ConverterRowModel> { get }
  var value: BehaviorRelay<String?> { get }
  func start()
  func stop()
}

final class ConverterViewController: UITableViewController {

  private typealias DataSource = RxTableViewSectionedAnimatedDataSource<CurrencySection>

  let vm: ConverterViewModelType
  private let bag = DisposeBag()
  private let dataSource: DataSource

  // MARK: - Initializers

  init(_ vm: ConverterViewModelType) {
    self.vm = vm
    dataSource = DataSource(
      configureCell: ConverterViewController.configureCell
    )
    super.init(style: .plain)
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: - UIViewController life cycle methods

  override func loadView() {
    super.loadView()
    tableView.rowHeight = 50
    tableView.register(ConverterCell.self)
    tableView.keyboardDismissMode = .onDrag
    tableView.allowsSelection = true
    tableView.allowsMultipleSelection = false
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    setupBindings()

    NotificationCenter.default.addObserver(
      self,
      selector: #selector(ConverterViewController.didEnterBackground),
      name: UIApplication.didEnterBackgroundNotification,
      object: nil
    )

    NotificationCenter.default.addObserver(
      self,
      selector: #selector(ConverterViewController.willEnterForeground),
      name: UIApplication.willEnterForegroundNotification,
      object: nil
    )
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    vm.start()
  }

  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    vm.stop()
  }

  // MARK: -

  private func setupBindings() {
    vm.title.asObservable()
      .bind(to: rx.title)
      .disposed(by: bag)
    vm.sections.asObservable()
      .bind(to: tableView.rx.items(dataSource: dataSource))
      .disposed(by: bag)

    tableView.rx.modelSelected(ConverterRowModel.self)
      .bind(to: vm.didSelect)
      .disposed(by: bag)

    tableView.rx.itemSelected
      .bind { [unowned self] i in
        let cell = self.tableView.cellForRow(at: i) as? ConverterCell
        _ = cell?.becomeFirstResponder()
        let deadlineTime = DispatchTime.now() + .milliseconds(100)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
          /// Must wait for 100ms in order for previous animation to finish.
          /// Otherwise we are not scrolled to the very top.
          self.tableView.scrollToRow(
            at: IndexPath(row: 0, section: 0),
            at: .top,
            animated: true
          )
        }
        cell?.bindInput(to: self.vm.value)
    }.disposed(by: bag)

    tableView.rx.itemDeselected
      .bind { [unowned self] i in
        let cell = self.tableView.cellForRow(at: i) as? ConverterCell
        _ = cell?.resignFirstResponder()
    }.disposed(by: bag)
  }

  // MARK: - Helpers

  static private func configureCell(
    dataSource: TableViewSectionedDataSource<CurrencySection>,
    tableView: UITableView,
    indexPath: IndexPath,
    item: ConverterRowModel
  ) -> UITableViewCell {
    let cell: ConverterCell = tableView.dequeueReusableCell(for: indexPath)

    cell.title = item.iso
    if item.iso.localized == item.iso {
      /// Subtitle for this ISO is not available.
      cell.subtitle = nil
    } else {
      cell.subtitle = NSLocalizedString(item.iso, comment: "")
    }

    if let mapped = mapping[item.iso] {
      cell.icon = Flag(countryCode: mapped)?.image(style: .circle)
    } else {
      cell.icon = nil
    }

    let formatter = NumberFormatter()
    formatter.maximumFractionDigits = 2
    formatter.minimumFractionDigits = 2

    let mappedExchangeRate = item.exchangeRate.asObservable().map { number -> String? in
      if number.isZero {
        return "0"
      } else {
        return formatter.string(from: NSNumber(value: number))
      }
    }

    cell.bind(to: mappedExchangeRate)

    return cell
  }

  // MARK: - Notification handlers

  @objc private func didEnterBackground() {
    print(#function)
    vm.stop()
  }

  @objc private func willEnterForeground() {
    print(#function)
    vm.start()
  }
}
