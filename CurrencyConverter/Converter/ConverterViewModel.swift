import RxSwift
import RxCocoa
import Networking

private let initialBaseCurrency = "USD"

final class ConverterViewModel: ConverterViewModelType {

  // MARK: - Dependencies

  private let dataService: ConverterDataServiceType

  // MARK: - Internal bindings

  private let scheduler: SchedulerType
  private let bag = DisposeBag()
  private var bag2 = DisposeBag()
  private let base: BehaviorRelay<CurrencyISO>
  private var rows: BehaviorRelay<[ConverterRowModel]>
  private var exchangeRates = Variable<ExchangeRates>(
    ExchangeRates(base: initialBaseCurrency,
                  rates: [:])
  )

  // MARK: - Outs

  let sections = BehaviorRelay<[CurrencySection]>(value: [])
  let title = BehaviorRelay<String>(value: "loading".localized)

  // MARK: - Ins

  let value = BehaviorRelay<String?>(value: "1.0")
  let didSelect = PublishSubject<ConverterRowModel>()

  // MARK: -

  init(dataService: ConverterDataServiceType,
       scheduler: SchedulerType = MainScheduler.instance) {
    self.dataService = dataService
    rows = BehaviorRelay(value: [])
    base = BehaviorRelay(value: initialBaseCurrency)
    self.scheduler = scheduler

    setupBaseBindings()
    setupRowsBindings()
    setupDidSelectBindings()

    rows.map { $0.isEmpty }
        .map { $0 ? "loading".localized : "" }
        .bind(to: title)
        .disposed(by: bag)

    let exchangeRatesAndValue = Observable.combineLatest(
      value.asObservable(),
      exchangeRates.asObservable()) {
      ($0, $1)
    }

    exchangeRatesAndValue
      .filter { !$0.1.rates.isEmpty }
      .subscribe(onNext: { [unowned self] in
      self.updateRows(rates: $0.1, value: $0.0.double)
    }).disposed(by: bag)
  }

  func start() {
    getNewRates()
  }

  func stop() {
    bag2 = DisposeBag()
  }

  /// Gets new rates from server, waits a second and calls itself again.
  /// Errors are ignored and it will keep retrying indefinately.
  private func getNewRates() {
    // Not the cleanest solution, I was unable to make it with with
    // `Observable<Int>.interval`.
    let t = dataService.getExchangeRates(for: base.value)
      .asObservable()
      .share()

    t.delay(1, scheduler: scheduler)
      .subscribe(
        onError: { [unowned self] _ in
          self.getNewRates()
      }, onCompleted: {
          self.getNewRates()
      }
    ).disposed(by: bag2)

    t.bind(to: exchangeRates)
      .disposed(by: bag2)
  }

  private func updateRows(rates: ExchangeRates, value: Double) {
    if rows.value.count != rates.rates.count + 1 {
      /// First load.
      var currencies = rates.rates.map {
        ConverterRowModel(iso: $0.key, value: $0.value * value)
      }.sorted { $0.iso < $1.iso }

      currencies.insert(
        ConverterRowModel(iso: base.value, value: value),
        at: 0
      )

      rows.accept(currencies)
    } else {
      rows.value.forEach {
        if base.value != $0.iso {
          if let rate = rates.rates[$0.iso] {
            $0.exchangeRate.value = rate * value
          }
        }
      }
    }
  }

  // MARK: -

  /// Setups bindings that are based on `base`.
  private func setupBaseBindings() {
    let baseWithSections = base.asObservable()
      .withLatestFrom(sections.asObservable()) { ($0, $1) }

    // If `base` changes, `items` is reordered in the first section.
    baseWithSections.map { base, sections -> [ConverterRowModel] in
      guard var rows = sections.first?.items else {
        return []
      }
      let firtstIndex = rows.firstIndex { $0.iso == base }
      guard let index = firtstIndex else {
        return rows
      }
      let selectedCurrency = rows.remove(at: index)
      rows.insert(selectedCurrency, at: 0)
      return rows
      }.map { [CurrencySection(items: $0)] }
      .bind(to: sections)
      .disposed(by: bag)

    /// If `base` changes, we change base for `exchangeRates` as well.
    base.asObservable()
      .withLatestFrom(exchangeRates.asObservable()) { ($0, $1) }
      .map { base, rates -> ExchangeRates in
        var copy = rates
        copy.base = base
        return copy
      }
      .bind(to: exchangeRates)
      .disposed(by: bag)
  }

  /// Setups bindings that are based on `rows`.
  private func setupRowsBindings() {
    // If `rows` change, it is mapped to `sections`.
    rows.map { [CurrencySection(items: $0)] }
      .bind(to: sections)
      .disposed(by: bag)
  }

  private func setupDidSelectBindings() {
    didSelect
      .map { $0.iso }
      .bind(to: base)
      .disposed(by: bag)

    didSelect
      .map { String($0.exchangeRate.value) }
      .bind(to: value)
      .disposed(by: bag)
  }
}
