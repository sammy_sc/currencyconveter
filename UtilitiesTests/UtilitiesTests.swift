import XCTest
@testable import Utilities

class OptionalStringTests: XCTestCase {

    func testDoubleConvertion() {
      var s: String? = "123"
      XCTAssertEqual(s.double, 123)

      s = nil

      XCTAssertEqual(s.double, 0)

      s = "0"

      XCTAssertEqual(s.double, 0)

      s = "1.13"

      XCTAssertEqual(s.double, 1.13)
    }
}
