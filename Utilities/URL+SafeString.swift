import Foundation

extension URL {
  public init(safeString: String) {
    guard let url = URL(string: safeString) else {
      fatalError("The url provided: \(safeString) could not be initialized")
    }
    self = url
  }
}
