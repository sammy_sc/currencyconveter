import UIKit

/// Following code are helper protocols and extensions to make it easier to work
/// with reusable identifiers.

public protocol ReusableView: class {}

public extension ReusableView where Self: UIView {
  static var reusableIdentifier: String {
    return String(describing: self)
  }
}

extension UITableViewCell: ReusableView {}

public extension UITableView {
  func register<T: UITableViewCell>(_: T.Type) {
    self.register(T.self, forCellReuseIdentifier: T.reusableIdentifier)
  }

  func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
    guard let cell = self.dequeueReusableCell(withIdentifier: T.reusableIdentifier, for: indexPath) as? T else {
      fatalError("could not deque a cell with identifier: \(T.reusableIdentifier)")
    }
    return cell
  }
}
