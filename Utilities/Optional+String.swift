import Foundation

public extension Optional where Wrapped == String {
  /// Tries to convert `Optional` string to `Double`.
  /// If `Optional` is `nil`, returns 0.
  /// If `String` fails conversion, returns 0.
  /// Otherwise return converted `String` to `Double`.
  public var double: Double {
    switch self {
    case .none: return 0
    case .some(let s): return Double.init(s) ?? 0
    }
  }
}
